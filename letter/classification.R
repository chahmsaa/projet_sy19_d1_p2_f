### SOURCE 
source("classifieurs.R")
source("fonctions.R")
options(warn=-1)

############################################################
# Library 
############################################################
library(MASS)
library(pROC)
library(mlbench)
library(caret)# confusion matrix
library(glmnet)
library(klaR)
#library(e1071)
library(corrplot)
library(nnet) # multinomial log linear models  
library(FNN)
library(naivebayes)
library(mda)
library(tree)         # Tree
library(rpart)        # Tree
library(randomForest) # Random Forest
library(gbm)          # Boosting
library(e1071)        # SVM
library(kernlab)    # ksvm
library(caret)        # CV

############################################################
# Load Data for classification 
############################################################
data <- read.table('..\\letters_train.txt')
names(data)[1] <- "Y"
data$Y <- as.factor(data$Y)

# mix up observations randomly
data <- data[sample(nrow(data)), ]

############################################################
# Nested and Non-nested Cross validation
############################################################
# creation de groupes pour la validation croisée 
n <-  nrow(data)
group.outer <- rep((1:10), (n/10)+1)[1:n]
idx.test.outer <- list()
idx.test.inner <- list()
rs.data.inner <- list()
for(i in 1:10){
  index.cv <- which(group.outer==i)
  idx.test.outer[[i]] <- index.cv
  n.inner <- n - length(index.cv)
  rs.data.inner[[i]] <- sample(n.inner)
  group.inner <- rep((1:10), (n.inner/10)+1)[1:n.inner]
  for(j in 1:10){
    index.cv <- which(group.inner==j)
    idx.test.inner[[j]] <- index.cv
  }
}

################################################################
## Classification
################################################################
err <-  matrix(0, 10, 13)
colnames(err) <- c("LR", "Ridge", "Lasso", "ElNet" , "RLM" , "RLN" ,"KNN", "NB", "QDA","LDA" , "MDA" , "FDA" , "RDA")

# ======== KNN classification ============ #
Kmax <- 15
K.sequences <- seq(1, Kmax, by = 2)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne (inner)
  knn.mse.kmin <- rep(0, length(K.sequences))
  for(k in 1:length(K.sequences)){
    for(j in 1:10){
      index.inner.cv <- idx.test.inner[[j]]
      data.train.x <- data.inner[-index.inner.cv, -1]
      data.train.y <- data.inner[-index.inner.cv, 1]
      data.test.x <- data.inner[index.inner.cv, -1]
      data.test.y <- data.inner[index.inner.cv, 1]
      model.reg <- knn(train=data.train.x, test=data.test.x, 
                       cl=data.train.y, k=k)
      errc <- 1 - sum(diag(table(data.test.y, model.reg)))/nrow(data.inner)
      knn.mse.kmin[k] <- knn.mse.kmin[k] + errc
    }
  } 
  idx.kmin <- which(min(knn.mse.kmin) == knn.mse.kmin)
  best.kmin <- K.sequences[idx.kmin]
  
  # validation our model with best model 
  data.train.x <- data.inner[, -1]
  data.train.y <- data.inner[, 1]
  data.test.x <- data.validation[, -1]
  data.test.y <- data.validation[, 1]
  model.knn <- knn(train=data.train.x, test=data.test.x, cl=data.train.y, k=best.kmin)
  n <- nrow(data.validation)
  err[i, c("KNN")] <- 1 - sum(diag(table(data.test.y, model.knn)))/n
}

# ======== Naive Bayes ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # inner cross-valition
  X <- as.matrix(data.inner[, -1])
  y <- as.factor(data.inner[, 1])
  model.naiveBayes <- naive_bayes(x=X,y=y)
  
  # validation
  #naiveBayes.pred.prob <- predict(naiveBayes.model,newdata=data.test[,-1],type="prob")
  err[i,c("NB")] <- testError(data.validation,model.naiveBayes)
}

# ======== Multinomial Logistic Linear - RLM  (neural networks) ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]

  # inner cross-valition
  model.multinom <- multinom(Y~.,data=data.inner)

  # validation
  # multinom.pred.prob <- predict(multinom.model,newdata=data.test[,-1],type="prob")
  err[i,c("RLM")] <- testError(test=data.validation,model=model.multinom)
}


# ======== Regularized Discriminant Analysis RDA  ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # inner cross-validation
  # grid search 
  cv_5_grid = trainControl(method = "cv", number = 10)
  fit_rda_grid = train(Y ~ ., data = data.inner, method = "rda", trControl = cv_5_grid)
  # random search 
  cv_5_rand = trainControl(method = "cv", number = 10, search = "random")
  fit_rda_rand = train(Y ~ ., data = data.inner, method = "rda",trControl = cv_5_rand, tuneLength = 9)
  if ( max(fit_rda_grid$results$Accuracy) > max(fit_rda_rand$results$Accuracy) ) {
    bestLambda = fit_rda_grid$bestTune[1,2]
    bestGamma  = fit_rda_grid$bestTune[1,1]
  }
  else {
    bestLambda = fit_rda_rand$bestTune[1,2]
    bestGamma  = fit_rda_rand$bestTune[1,1]
  }
  
  # validation
  model.rda<-rda(Y~.,data.inner,lambda = bestLambda , gamma=bestGamma)   
  err[i,c("RDA")] <- testError(test=data.validation,model=model.rda,type="rda") 
}


# ======== Quadratic Discriminant Analysis QDA  ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.qda<-qda(Y~.,data.inner) 
  
  # validation
  err[i,c("QDA")] <- testError(test=data.validation,model=model.qda,type="rda") 
}
err<-cbind(err,c(0, 0,0, 0,0, 0,0, 0,0, 0))
colnames(err) <- c("LR", "Ridge", "Lasso", "ElNet" , 
                   "RLM" , "RLN" ,"KNN", "NB", "QDA","LDA" , 
                   "MDA" , "FDA" , "RDA","bagging","ksvm","QDA2")
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.qda<-qda(Y~.*.,data.inner) 
  #1 2 3 12 13 23
  
  # validation
  err[i,c("QDA2")] <- testError(test=data.validation,model=model.qda,type="rda") 
}

# ======== Linear Discriminant Analysis LDA  ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.lda<-lda(Y~.,data.inner) 
  
  # validation
  err[i,c("LDA")] <- testError(test=data.validation,model=model.lda,type="rda") 
}

# ======== Mixture Discriminant Analysis MDA  ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.mda<-mda(Y~.,data.inner) 
  
  # validation
  err[i,c("MDA")] <- testError(test=data.validation,model=model.mda) 
}


# ======== Flexible Discriminant Analysis FDA  ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.fda<-fda(Y~.,data.inner) 
  
  # validation
  err[i,c("FDA")] <- testError(test=data.validation,model=model.fda) 
}

# ======== Linear Regression LR  ============ #
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.lm<-lm(Y~.,data.inner) 
  
  # validation
  err[i,c("LR")] <- testError(test=data.validation,model=model.lm) 
}



# =================================================================== #
#                     LOGISTIC REGRESSION                             # 
# =================================================================== #
# http://www.sthda.com/english/articles/36-classification-methods-essentials/149-penalized-logistic-regression-essentials-in-r-ridge-lasso-and-elastic-net/ 
# https://daviddalpiaz.github.io/r4sl/regularization.html#ridge-regression
# LASSO // RIDGE // ELASTIC NET ? 

err_regression <-  matrix(0, 10, 3)
colnames(err_regression) <- c("Ridge Regression", "Lasso Regression", "ElasticNet Reg.") 

# Ridge Regression 
for(i in 2:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne (inner)
  x <- as.matrix(data.inner[, -1])
  y <- as.factor(data.inner[, 1])
  fit_ridge = glmnet(x, y, alpha = 0, family = "multinomial")
    
  # Inner Cross-Validation with 10 folds by default using cv.glmnet 
  fit_ridge_cv = cv.glmnet(x, y, alpha = 0,family = "multinomial" ,nfolds=3)
    
  best.lambda = fit_ridge_cv$lambda.min
  
  # validation model 
  X.train <- as.matrix(data.inner[, -1])
  y.train <- data.inner[, 1]
  X.test <- as.matrix(data.validation[, -1])
  y.test <- data.validation[, 1]
  pred <-predict( fit_ridge_cv, newx = X.test, s = "lambda.min", type = "class")
  length(which(pred!=y.test))
  err[i,c("Ridge")] <-length(which(pred!=y.test))/nrow(X.test)
  err_regression[i,c("Ridge Regression")] <-err[i,c("Ridge")]
}

# Lasso Regression 
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne (inner)
  x <- as.matrix(data.inner[, -1])
  y <- as.factor(data.inner[, 1])
  #fit_lasso = glmnet(x, y, alpha = 1, family = "multinomial")
  fit_lasso_cv = cv.glmnet(x, y, alpha = 1,family = "multinomial" ,nfolds=3)
  #plot(fit_lasso_cv)
  best.lambda = fit_lasso_cv$lambda.min
  
  # validation model 
  X.train <- as.matrix(data.inner[, -1])
  y.train <- as.factor(data.inner[, 1])
  X.test <- as.matrix(data.validation[, -1])
  y.test <- as.factor(data.validation[, 1])
  pred <-predict( fit_lasso_cv, newx = X.test, s = "lambda.min", type = "class")
  fit_lasso.coef=predict(fit_lasso_cv,type="coefficients",s= "lambda.min")
  length(which(pred!=y.test))
  err[i,c("Lasso")] <-length(which(pred!=y.test))/nrow(X.test)
  
  err_regression[i,c("Lasso Regression")] <-length(which(pred!=y.test))/nrow(X.test)
 
}

# Elastic Net Regression 
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne (inner)
  x <- as.matrix(data.inner[, -1])
  y <- as.factor(data.inner[, 1])
  cv_5 = trainControl(method = "cv", number = 5)
  fit_elnet_cv = train(Y ~ ., data = data.inner, method = "glmnet", trControl = cv_5 )
  best.alpha <- fit_elnet_cv$bestTune[1,1]
  best.lambda <- fit_elnet_cv$bestTune[1,2]
  
  # validation model 
  X.train <- as.matrix(data.inner[, -1])
  y.train <- data.inner[, 1]
  X.test <- as.matrix(data.validation[, -1])
  y.test <- data.validation[, 1]
  # Fit the final model on the training data
  model.elnet <- glmnet(X.train, y.train, family = "multinomial", alpha = best.alpha , lambda = best.lambda)
  pred <- predict(model.elnet,X.test, type="class")
  confusion <- table(y.test,pred)
  err_regression[i,c("ElasticNet Reg.")] <-1-sum(diag(confusion))/nrow(X.test)
  err[i,c("ElNet")] <-1-sum(diag(confusion))/nrow(X.test)
}
###############################################################################
#                                  Bagging                                    #
###############################################################################
library(rpart)        # Tree
library(randomForest) # Random Forest
err<-cbind(err,c(0, 0,0, 0,0, 0,0, 0,0, 0))
colnames(err) <- c("LR", "Ridge", "Lasso", "ElNet" , 
                   "RLM" , "RLN" ,"KNN", "NB", "QDA","LDA" , 
                   "MDA" , "FDA" , "RDA","bagging")
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  # Fit a bagging model
  model.bagging <- randomForest(Y~.,data.inner,
                              mtry=16,importance=TRUE)
  # validation
  err[i,c("bagging")] <- testError(test=data.validation,model=model.bagging) 
}
###############################################################################
#                               Random Forest                                 #
###############################################################################
err<-cbind(err,c(0, 0,0, 0,0, 0,0, 0,0, 0))
colnames(err) <- c("LR", "Ridge", "Lasso", "ElNet" , 
                   "RLM" , "RLN" ,"KNN", "NB", "QDA","LDA" , 
                   "MDA" , "FDA" , "RDA","bagging","ksvm","QDA2","RForest")
 
library(randomForest)

err.nrf.mse<- rep(0, 10)
err.nrf.Pmin <- rep(0,10)
P.sequences <- c(2,3,4,5)
#Verifier à quoi sert le p

for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne (inner)
  rdf.mse.Pmin <- rep(0, length(P.sequences))
  for(P in 1:length(P.sequences)){
    for(j in 1:1){
      index.inner.cv <- idx.test.inner[[j]]
      data.train <- data.inner[-index.inner.cv,]
      data.test<- data.inner[index.inner.cv,]
      model.rdf <- randomForest(Y~.*., data=data.train, ntree = 100, mtry=P.sequences[P])
      pred <- predict(model.rdf, newdata=data.test)
      errc<-mean(data.test[,1] != pred)
      rdf.mse.Pmin[P] <- rdf.mse.Pmin[P] + errc
    }
  }
  idx.Pmin <- which(min(rdf.mse.Pmin) == rdf.mse.Pmin)
  best.Pmin <- P.sequences[idx.Pmin]
   
  # validation our model with best model 
  model.rdf <- randomForest(Y~.*., data=data.train, ntree = 100, 
                            mtry=best.Pmin)
  n <- nrow(data.validation)
  pred<-predict(model.rdf,newdata=data.validation)
  table(data.validation[,1],pred)
  
  err[i, c("RForest")] <- mean(data.validation[,1] != pred)
}



###############################################################################
#                                   SVM                                       #
###############################################################################
#----------------------------
# Q3: SVM non linéaire
#----------------------------

# SVM avec noyau gaussien
# Réglage de C par validation croisée

CC<-c(0.001,0.01,0.1,1,10,100,1000,10e4)
N<-length(CC)
M<-10 # nombre de répétitions de la validation croisée
errc<-matrix(0,N,1)
#ksvm(Y~., letter.recognition[train,],type="spoc-svc",kernel="rbfdot",kpar="automatic",C=CC[4] )
for(k in 1:1){
  for(i in 1:N){
    svmfit.lap<-ksvm(Y~., data[1:2000,],type="C-svc",kernel="rbfdot",
                         C=CC[i])
  pred<-predict(svmfit.lap,newdata=data[9001:10000,])
 
  errc[i,]<-mean(data[9001:10000,1] != pred)
   
  }
}
# svm.Err
#[1] 0.96137781 0.85495643 0.27145705 0.11304947 0.05890193
#[6] 0.05725211 0.05756695 0.05773227
# c.opt = 10
ksvm.Err<-rowMeans(errsvm)
plot(CC,errc,type="b",log="x",xlab="C",ylab="error",main="rbfdot")
plot(CC,errc,type="b",log="x",xlab="C",ylab="error",main="laplacedot")

# Calcul de l'erreur de test avec la meilleure valeur de C
#svmfit.rbf<-ksvm(Y~., letter.recognition[train,],type="C-svc",kernel="rbfdot",C=10)
svmfit.rbf<-ksvm(Y~., letter.recognition[train,],type="C-svc",kernel="laplacedot",C=10)
pred<-predict(svmfit.rbf,newdata=letter.recognition[-train,-1])
table(letter.recognition[-train,1],pred)
ksvm.err<-mean(letter.recognition[-train,1] != pred)
print(ksvm.err) #0.05220522
ksvm.test.rate = 1-ksvm.err

ksvm.fit <- train(Y~., letter.recognition,method="svmRadial",
                  trControl = trainControl(
                    method = "cv",number = 5
                  ))
###cv
err<-cbind(err,c(0, 0,0, 0,0, 0,0, 0,0, 0))
colnames(err) <- c( "Ridge", "Lasso",  
                    "RLM" , "KNN", "NB", "QDA","LDA" , 
                    "MDA" , "FDA" , "RDA","bagging","svm.rbf","QDA2","RForest","svm.lap")
C.sequences <- c(10,100)

for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne (inner)
  svm.mse.Cmin <- rep(0, length(C.sequences))
  for(C in 1:length(C.sequences)){
    for(j in 1:1){
      index.inner.cv <- idx.test.inner[[j]]
      data.train <- data.inner[-index.inner.cv,]
      data.test<- data.inner[index.inner.cv,]
      #svmfit.rbf <- ksvm(Y~., data.train
      #                  ,type="C-svc",kernel="rbfdot",C=C)

      
      svmfit.rbf <- ksvm(Y~., data.train
                         ,type="C-svc",kernel="laplacedot",C=C)
      pred<-predict(svmfit.rbf,newdata=data.test)
      table(data.test[,1],pred)
      errc<-mean(data.test[,1] != pred)
      svm.mse.Cmin[C] <- svm.mse.Cmin[C] + errc
    }
  }
  idx.Cmin <- which(min(svm.mse.Cmin) == svm.mse.Cmin)
  best.Cmin <- C.sequences[idx.Cmin]
  # best.Cmin <-10
  # validation our model with best model 
  model.svm <- ksvm(Y~., data.inner
                    ,type="C-svc",kernel="laplacedot",C=best.Cmin)
  n <- nrow(data.validation)
  pred<-predict(model.svm,newdata=data.validation)
  table(data.validation[,1],pred)
  
  err[i, c("svm.lap")] <- mean(data.validation[,1] != pred)
}

#########gam
library(gam)
library(leaps)
err<-cbind(err,c(0, 0,0, 0,0, 0,0, 0,0, 0))
colnames(err) <- c( "Ridge", "Lasso",  
                   "RLM" , "KNN", "NB", "QDA","LDA" , 
                   "MDA" , "FDA" , "RDA","bagging","ksvm","QDA2","RForest","GAM")
for(i in 1:1){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  
  # cross-validation interne 
  model.gam = gam(Y ~ ., data = data.inner,family = "gaussian") 
  
  # validation
  err[i,c("GAM")] <- testError(test=data.validation,model=model.gam) 
}
 
##########################################################

plot.cv.error(err_regression,c("Ridge Regression", "Lasso Regression", "ElasticNet Reg.") )
boxplot(err_regression)

plot.cv.error(err, c("Régression Logistique", "Régression Ridge", 
                     "Régression Lasso", "Régression Elastic Net" ,
                     "Régression logistique multinomiale", "Régression logistique normale",
                     "K plus proches voisins", "Näive Bayes", "Analyse discriminante quadratique",
                     "Analyse discriminante linéaire", "Analyse discriminante mixte", 
                     "Analyse discriminante flexible", 
                     "Analyse discriminante régularisée"))

boxplot(err,main="Répartition des probabilités d'erreurs")

best.modelClass <- ksvm(Y~., data
                  ,type="C-svc",kernel="laplacedot",C=100)
 
save(best.modelClass,file="env.RData")
#source("predicteur.R")
prediction_letter <- function(dataset) {
  # Chargement de l’environnement
  load("env.Rdata")
  pred <- predict(best.modelClass,newdata=dataset)
  return(pred)
}
predict=prediction_letter(data)
mean(data[,1] != predict)











options(warn=0)

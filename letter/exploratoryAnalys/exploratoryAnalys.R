#library(tidyverse)
library(ggplot2)
library(reshape2)
library(corrplot)
library(FactoMineR)
library(factoextra)
library(MASS)
library(lmtest)

nrow(letter.recognition)
ncol(letter.recognition) 
str(letter.recognition)
summary(letter.recognition) 

# ========================================= #
#                 CORR PLOT                 #
# ========================================= #
corrplot(cor(letter.recognition[,-1]), type = "upper", order = "hclust", tl.col = "black", tl.srt = 45)

# ========================================= #
#                  BAR PLOT                 #
# ========================================= #
boxplot(letter.recognition)
#heatmap(letter.recognition[,-1])


# ========================================= #
#               Multiple Boxplot            #
# ========================================= #
reshapeData <- melt(letter.recognition,id.var="Y") 

ggplot(data = reshapeData , aes(x=variable, y=value)) + geom_boxplot(aes(fill=Y))

p <- ggplot(data = reshapeData, aes(x=variable, y=value)) + 
             geom_boxplot(aes(fill=Y))
p + facet_wrap( ~ variable, scales="free")

# ========================================= #
#                  PCA == ACP               #
# ========================================= #
#https://blog.csdn.net/LuohenYJ/article/details/97950522
model.pca <- PCA(letter.recognition[,-1],scale.unit = TRUE, graph = TRUE)
fviz_pca_var(model.pca, col.var = "cos2",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"), 
             repel = TRUE # Avoid text overlapping
)
print(model.pca)

eig.val <- get_eigenvalue(model.pca)
fviz_eig(model.pca, addlabels = TRUE, ylim = c(0, 30),main="Méthode du coude avec l'ACP")

var <- get_pca_var(model.pca)
# Coordinates
head(var$coord)
# Cos2: quality on the factore map
head(var$cos2)
# Contributions to the principal components
head(var$contrib)
# Coordinates of variables
head(var$coord, 4)

#Plot variables : 
fviz_pca_var(model.pca, col.var = "black")
# You can visualize the cos2 of variables on all the dimensions using the corrplot package:
corrplot(var$cos2, is.corr=FALSE)
# Total cos2 of variables on Dim.1 and Dim.2
fviz_cos2(model.pca, choice = "var", axes = 1:2)


 
# ========================================= #
#                  COOK DISTANCE            #
# ========================================= #
# 
library(olsrr)
library(performance)

model <- lm(Y ~ ., data=letter.recognition) #lm(mpg ~ disp + hp + wt, data = mtcars)
ols_plot_cooksd_chart(model)

mod <- lm(Y ~ ., data=letter.recognition)
cooksd <- cooks.distance(mod)

check_outliers(letter.recognition, method = c("cook", "pareto"))

plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance")  # plot cook's distance
abline(h = 4*mean(cooksd, na.rm=T), col="red")  # add cutoff line
text(x=1:length(cooksd)+1, y=cooksd, labels=ifelse(cooksd>4*mean(cooksd, na.rm=T),names(cooksd),""), col="red")  # add labels
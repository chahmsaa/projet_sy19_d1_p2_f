---
title: "phonemes "
output: pdf_document

---


### Analyse Exploratoire 

Dans un premier temps, avant d'appliquer des modèles pour apprendre sur les données à notre disposition, nous allons les analyser pour en extraire une structure et des informations intéressantes. Le jeu de données est constitué de 256 prédicteurs et une variable de sortie `Y` qui est la classe recherchée. Cette classe est composée de 5 types : `aa`,`ao` , `dcl`, `iy` et `sh`. Les prédicteurs quant à eux sont des variables quantitatives, avec des valeurs réelles. Cet apprentissage des données est d'autant plus difficile qu'aucune information complémentaire est donnée par rapport aux prédicteurs. Nous ne pouvons donc pas nous appuyer sur des connaissances extérieures ou sur un domaine particulier, mais seulement les informations inhérentes. Cette analyse exploratoire sera justement un très bon moyen d'en savoir plus sur les données manipulées. 
La dimension $p=256$ qui est assez importante rend l'affichage très difficile, mais nous pouvons regarder un peu la répartition de ces cinq classes : 

![Diagramme des classes ](./class_distribution.png){ width=40% }

Cette première approche nous permet d'observer que les cinq classes sont réparties de manière assez égale. Donc on peut utiliser le metric *Accuracy* pour évaluer la performance des modèles.

### Data engineering
Vu que le nombre des prédictuer est assez grand, nous allons donc penser à extraire les prédicteurs les plus parlants, qui expliquent le mieux la classe recherchée. Pour cela, on applique l'ACP : *Analyse en Composante Principale* (PCA). On remarque très rapidement, que cette approche semble concluante, avec quelques dimensions dominantes ou très explicatives.
Ici, pour évaluer les modèles d'apprentissage, nous allons choisir les `100` premiers variables pour l'instant. Et après avoir trouvé le meilleur modèle, on va appliquer la méthode *validation croisée* pour choisir le meilleur nombre de prédicteur. 

![Analyse en composante principale : le pourcentage expliqué de la variance selon les dimensions](./pca.png){ width=30% }![Intervalle de confiance des erreurs avant PCA](./ci_sans_pca.png){ width=30% }   ![Intervalle de confiance des erreurs après PCA](./ci_new.png){ width=30% }

On peut constater que l'applique du PCA globalement réduit les erreurs de prédiction en comparant les deux shcémas au-dessus.


### Application des méthodes d'apprentissage 

Dans cette partie, nous allons chercher un modèle qui apprend et permet d'estimer la classe de la manière la plus fiable possible. Chaque modèle appris sera réalisé sur les $p=100$ nouveaux prédicteurs à disposition. Pour évaluer les différents modèles réalisées, nous allons estimer l'erreur directe de prédiction avec la méthode de validation croisée imbriquée (`nested cross-validation`). Dans notre cas, nous avons divisé les données en 10 parties `n-folds=10` avec des données externes et internes. Pour certains modèles, les données internes ne seront pas utiles, car il n'y a pas d'hyperparamètre à estimer. 

Les `18` modèles qu'on a appliqués sont les suivantes : *régression logistique*, *K plus proches voisins (KNN)*, *naïve Bayes*,*analyse discriminante linéaire(LDA)*,  *analyse discriminante quadratique(QDA)*, *analyse discriminante régularisée(RDA)*, *analyse discriminante flexible(FDA)*, *régression logistique régularisé(ridge,lasso,elastic net)*,*Decision Tree*,*Decision Tree pruned*, *bagging*, *random Forest*, *support vector machine(SVM)*, *réseau de neurones Profonds régularisé* 
La méthode régression logistique multinomiale a été appliquée sur le jeu de données. C'est un algorithme de classification probabiliste en généralisant la méthode de régression logistique à plusieurs classes.

La méthode des *K plus proches voisins (KNN)* permet d'assigner à une observation une classe en fonction de ses voisins dans un espace euclidien de dimension $p=100$. Ici ce qui nous intéresse est de choisir le meilleure K avec une validation croisée interne. 

La méthode *naïve Bayes* est une méthode qui utilise le théorème de Bayes de manière à choisir la classe avec la plus haute probabilité conditionnelle. Pour appliquer ce modèle, on fait l'hypothèse d'une forte indépendance conditionnelle et que les matrices de covariances sont égales entre-elles. Cela simplifie énormément le modèle, mais cela ne signifie pas pour autant qu'on obtiendra de mauvaises performances. 

Le modèle *analyse discriminante linéaire* (LDA) quant à lui réalise des hypothèses moins fortes que naïve Bayes avec l'homscédasticité et la normalité. On peut aussi utiliser le modèle *analyse discriminante quadratique* (QDA) qui ne fait plus l'hypothèses sur l'homoscédasticité mais suppose qu'il y a une matrice de variance par variable et non plus, une seule commune. Nous avons aussi appliqué l'*analyse discriminante régularisée* (RDA) qui permet avec sa règle de décision $\lambda$ et $\gamma$ de varier entre les deux. Pour sélectionner les meilleures valeurs des paramètres de régularisation, on utilise la validation croisée interne. Nous avons aussi appliqué des variantes de ces modèles avec l'*analyse discriminante flexible* (FDA). Le modèle FDA utilise des combinaisons non-linéaires des prédicteurs.

La méthode de *régression linéaire* a été appliquée au jeu de données pour rechercher des frontières linéaires entre les classes. Elle est appliquable pour des modèles binaires et multi-classes. Dans notre cas, nous devons obtenir cinq régions distinctes. On utilise la *régression ridge, lasso et elastic net*. L'objectif est de trouver les meilleurs hyperparamètres $\alpha$ et $\lambda$ avec une validation croisée interne. 

Dans la méthode *Decision Tree*, on utilise *rpart.control(xval = 10, minbucket = 10,cp=0.1)* pour effectuer quelques réglages sur l'arbre. *xval* est une validation croisée 10 fois. *minbucket* est le nombre minimum d'échantillons de nœuds de feuilles.*cp* est appelé le paramètre de complexité, qui fait référence à la complexité d'un point, pour chaque étape de la scission, l'ajustement du modèle doit améliorer le degré de qualité de l'ajustement. Après cela, pour éviter le overfitting, on applique la méthode "prune" dans l'arbre. Les deux arbres ci-dessous sont un des arbres non coupés et un des arbres coupés parmi les 10 fois validation croisée :

![Decision Tree](./dt.png){ width=30% }     ![Decision Tree Pruned](./dt_prune.png){ width=30% }


Néanmoins les arbres de décision peuvent souffrir d'une variance élevée. Cela signifie que si nous divisons les données de formation en deux parties aléatoires et que nous plaçons un arbre de décision pour les deux, nous pouvons obtenir des résultats assez différents. Le bootstrap clustering, ou bagging, est une procédure générale pour réduire la variance des méthodes d'apprentissage. On applique donc la méthode *bagging* en utilisant la fonction randomforest() dans R et on 
Nous fixons le paramètre *mtry = p* qui signifie le nombre de variables échantillonnées au hasard en tant que candidats à chaque fractionnement.
Bien que le *bagging* améliore les performances de prédiction des arbres de décision généraux en réduisant la variance, il présente d'autres inconvénients : les arbres basés sur le bagging sont corrélés et saturent la précision des prédictions. Les *forêts aléatoires* dé-corrélent tous les arbres par des perturbations aléatoires, donc les forêts aléatoires ont de meilleures performances que le Bagging. Contrairement à l'bagging, les forêts aléatoires utilisent un prédicteur d'échantillon aléatoire avant que chaque nœud ne se divise lors de la construction de chaque arbre. Parce qu'à la base, les forêts aléatoires sont toujours les mêmes que le bagging, leur variance est réduite. L'avantage le plus significatif de l'utilisation de l'approche de la forêt aléatoire est qu'elle donne plus d'informations pour réduire le biais dans les valeurs ajustées et la partition estimée. En fonction de la validation croisée et la recherche par grille , on obtient le meilleur hyper patamètre *mtry* et l'applique dans la prédiction. 

La méthode *SVM* utilise les fonctions internes du noyau au lieu de la mappe non linéaire vers des espaces à dimensions plus élevées. L'hyperplan optimal pour le partitionnement de l'espace des caractéristiques est l'objectif du SVM, et l'idée de maximiser la marge de classification est le cœur de la méthode SVM. La fonction de décision finale du SVM n'est déterminée que par un petit nombre de vecteurs de soutien, et la complexité du calcul dépend du nombre de vecteurs de soutien plutôt que de la dimensionnalité de l'espace échantillon. Cela permet d'éviter la "catastrophe dimensionnelle" dans un sens. Ici, on utilise trois fonctions de noyau : linéaire, polynomiale et gaussienne. Pour obtenir les meilleurs hyperparamètres comme ¨*C*(facteur de pénalité), *gamma* etc., les mêmes opérations sont faites : validation croiséé et recherche par grille.

Pour terminer, le dernier modèle est *réseau de neurones profonds régularisé*(DNN).Au vu des résultats, l'apprentissage approfondi comme DNN fonctionne très bien et il est très doué pour apprendre. L'apprentissage approfondi comporte de nombreuses couches de réseaux de neurones de grande largeur, qui peuvent théoriquement se connecter à n'importe quelle fonction, ce qui permet de résoudre des problèmes très complexes. Ici, nous utilisons un réseau neuronal à quatre couches au lieu d'un réseau neuronal peu profond. Comme il existe un ensemble de caractéristiques dans le réseau plus profond, plus le réseau est profond, plus les informations sémantiques des caractéristiques extraites sont avancées. 
La construction de réseaux : 

```{r eval=FALSE}
model %>% layer_dense(units=p,activation = "relu",input_shape = p) %>%
  layer_dense(units=2*p, activation = "relu") %>%
  layer_dropout(rate = 0.5)%>%
  layer_dense(units= 2*p, activation = "relu") %>%
  layer_dropout(rate = 0.5) %>%
    layer_dense(units=5,activation = "softmax") 
model  %>% compile(loss='categorical_crossentropy',
                  optimizer='adam',metrics = c('accuracy'))
```
On utilise une couche d'entrée, une couche de sortie et deux couches cachées. Pour les trois premiers couches, la fonction d'activation est *relu* et vu que c'est le cas de multiclass, on choisi *softmax* pour classifier dans la dèrnière couche.
En même temps, on utilise le *crossentropy*(loss function) et la méthode *adam* pour la mise à jour des paramètres de réseaux. La performance de ce modèle est très bien : 100% accuracy dans les training datas mais pas si bien sur les testing datas. C'est overfitting. Pour éviter cela, la méthode "dropout" est appliquée avec le taux 50%. Les schémas ci-dessous illustrent les performances du réseau de neurones profonds non régularisé et du réseau de neurones profonds régularisé : 

![réseau de neurones profonds non régularisé](./nnet_avant_dropout.png){ width=40% }             ![réseau de neurones profonds régularisé](./nnet_dropout.png){ width=40% }



Nous pouvons constater qu'après le dropout, les performances sur les training datas sont moins bonnes, mais cela évite en fait l'overfitting, de sorte que les performances sur les testing datas sont meilleures.


### Conclusion 
Pour afficher les performances des différentes méthodes appliquées sur le jeu de données, plusieurs solutions sont à notre disposition. Ici, on va évaluer la performance sur l'erreur de classification calculée avec la formule :
$$
Err_\mathcal{T}=\frac{1}{m}\sum_{i=1}^m I[y_i \ne \hat{y_i}]
$$
L'erreur de classification est réalisée sur une validation croisée imbriquée. Cette validation croisée permet de calculer l'erreur à l'aide de toutes les observations de notre jeu de données et de répéter plusieurs fois cette démarche pour obtenir une valeur non-biaisée grâce aux tests de validations externes. Les données ont été partitionnées en 10 sous-ensembles. Un schéma illustre les intervalles de confiances de ces erreurs pour chaque modèle : 

![Intervalle de confiance des erreurs après PCA](./ci_new.png){ width=40% } ![Validation croisée pour choisir le p](./dim.jpeg){ width=40% }

D'après le schéma des intervalles de confiances de ces erreurs, on peut constater que les trois premiers meilleurs modèles sont FDA, Lasso et RDA. Après avoir appliqué la méthode *hold out*, en comparant les taux d'erreur des testing datas, nous choisissons *RDA* comme le meilleur modèle. En plus, pour chercher le plus approprié dimension des prédicteurs après ACP, la validation croisée est appliqué.Le schéma ci-dessus à droite illustre la recherche de p avec le plus bas du taux d'erreur(p=190) et on obtient le taux des erreurs 0.0653 en testing data qui n'est pas mal.










